APP_NAME=jquery-mobile
PACKAGE=acf-$(APP_NAME)
VERSION=0.0.1

APP_DIST=\
	*.js\

STATIC_DIST=\
	*.css\
	images\

EXTRA_DIST=README Makefile config.mk

DISTFILES=$(APP_DIST) $(STATIC_DIST) $(EXTRA_DIST)

TAR=tar

P=$(PACKAGE)-$(VERSION)
tarball=$(P).tar.bz2
install_dir=$(DESTDIR)/$(wwwdir)/js
static_dir=$(DESTDIR)/$(staticdir)/jqmobile

all:
clean:
	rm -rf $(tarball) $(P)

dist: $(tarball)

install:
	mkdir -p "$(install_dir)"
	cp -a $(APP_DIST) "$(install_dir)"
	mkdir -p "$(static_dir)"
	cp -a $(STATIC_DIST) "$(static_dir)"

$(tarball):	$(DISTFILES)
	rm -rf $(P)
	mkdir -p $(P)
	cp -a $(DISTFILES) $(P)
	$(TAR) -jcf $@ $(P)
	rm -rf $(P)

# target that creates a tar package, unpacks is and install from package
dist-install: $(tarball)
	$(TAR) -jxf $(tarball)
	$(MAKE) -C $(P) install DESTDIR=$(DESTDIR)
	rm -rf $(P)

include config.mk

.PHONY: all clean dist install dist-install
